let num = Number(prompt('Введіть число'));

while (num % 1 !== 0) {
    num = Number(prompt('Введіть ціле число'));
}

for (let i = 1; i <= num; i++) {
    if (i % 5 === 0) {
        console.log(i);      
    } 
}

if (num < 5) {
    console.log("Sorry, no numbers");   
}

// Необов'язкове завдання

let m = Number(prompt('Введіть число m'));
let n = Number(prompt('Введіть число n'));

while (m % 1 !== 0 || m == '') {
    m = Number(prompt('Помилка, введіть ціле число m'));
}

while (n % 1 !== 0 || n == '') {
    n = Number(prompt('Помилка, введіть ціле число n'));
}

if (m > n) {
    min = n;
    max = m;
} else {
    min = m;
    max = n;
}

next:
for (let i = min; i <= max; i++) {
    if (i <= 1) continue;
    for (let j = 2; j < i; j++) {
        if (i % j == 0) continue next;
    }
    console.log(i);
}






